const express = require(`express`)
const router = express.Router()
const userController = require(`../controller/userController`)
const auth = require(`../auth`)
const { route } = require("./courseRoute")

router.get(`/`, userController.getAll)

router.post(`/register`, userController.register)

router.get(`/checkEmail`, userController.checkEmail)

router.get(`/login`, userController.login)

router.get(`/details`, auth.verify, userController.detail)

router.put(`/update/:id`, userController.update)

router.delete(`/delete`, userController.delete)

router.post(`/enroll`, userController.enroll)

// route to nroll a user to a course
// Route to enroll user to a course










// router.post("/enroll", auth.verify, (req, res) => {
// 	let data = {
// 		userId : req.body.userId,
// 		courseId : req.body.courseId,
//         isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}

// 	userController.enroll(data).then(resultFromController => res.send(resultFromController));

// })



module.exports = router;