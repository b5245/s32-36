const express = require(`express`)
const router = express.Router()
const courseController = require(`../controller/courseController`)
const auth = require(`../auth`)

router.post(`/`, auth.verify, courseController.addCourse)

router.get(`/`, courseController.getAll)

router.delete(`/deleteCourse`, auth.verify, courseController.delete)

router.put(`/update/:id`, auth.verify, courseController.updateCourse)

router.put(`/:id`, auth.verify, courseController.archive)


module.exports = router;