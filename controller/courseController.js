const Course = require(`../models/Course`)
const bcrypt = require(`bcrypt`)
const auth = require(`../auth`)
const User = require("../models/User")
const { off } = require("../models/Course")


module.exports = {

    addCourse: (req,res) => {
       
        const data = {isAdmin: auth.decode(req.headers.authorization).isAdmin}
        
       
        if(data.isAdmin){
            const newCourse = new Course({
                name : req.body.name,
			    description : req.body.description,
			    price : req.body.price
            })

           Course.findOne({name: req.body.name}).then(result => { 
            if(result == null){
                newCourse.save().then(result => {
                res.send(result)
                })
                
            }
            else{
                res.send(`Course is already Existing`)
            }
           })

        }
        else{
            res.send(`User unauthorized`)
        }
    },

    getAll: (req,res) => {
       
        Course.find().then(result => {
                   
            // for(let i = 0; i < result.length; i++){
            //     if(result[i].isActive == false){
            //         result[i].description = null
            //         result[i].price = null
            //         result[i].createdOn = null
            //         result[i].enrollees = null
            //         res.send(result[i]);
                    
                   
                    
            //     }
            //     else{
            //         res.send(result[i]);
                   
                    
            //     }
            // }
            res.send(result)
           
        })
    },

    delete: (req,res) => {
        const data = {isAdmin: auth.decode(req.headers.authorization).isAdmin}
        if(data.isAdmin){
            Course.findByIdAndDelete(req.body.id).then(result => {
            res.send(`Deleted`)
        })
        }
        else{
            res.send(`Admin required`)
            
        }
        
    },

    updateCourse: (req,res) => {
        const data = {isAdmin: auth.decode(req.headers.authorization).isAdmin}

        if(data.isAdmin){
            Course.findById(req.params.id).then(result => {
                if(result == null){
                    res.send(`User not found`)
                }
                else{
                    Course.findByIdAndUpdate({_id: req.params.id}, {$set: {
                            description: req.body.description,
                            price: req.body.price
                        }}).then(result => {
                            res.send(`Course Updated`)
                        })
                }
            })
        }
        else{
            res.send(`Admin Required`)
        }
        
    },

    archive: (req,res) => {
        const data = {isAdmin: auth.decode(req.headers.authorization).isAdmin}
        if(data.isAdmin){
            Course.findById(req.params.id).then(result => {
            if(result == null){
                res.send(`Course does not exist`)
            }
            else{
                Course.findByIdAndUpdate({_id: req.params.id}, {$set: {
                    isActive: req.body.isActive
                }}).then(result => {
                    if(req.body.isActive == true){
                        res.send(`Course now active`)
                    }
                    else{
                        res.send(`Course Archived`)
                    }
                })
            }
        })
    }
    else{
        res.send(`Admin required`)
    }
    }

}