const User = require(`../models/User`)
const Course = require(`../models/Course`)
const bcrypt = require(`bcrypt`)
const auth = require(`../auth`)

module.exports = {

    getAll: (req, res) => {
        User.find().then(result => {
            res.send(result)
        })
    },

    register: (req,res) => {
        const newUser = new User({
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    email: req.body.email,
                    password: bcrypt.hashSync(req.body.password, 10),
                    mobileNo: req.body.mobileNo
                    
                })

        User.findOne({email: req.body.email}).then(result => {
            if(result == null){
                newUser.save();
                res.send(newUser)
            }
            else{
                res.send(`Email already taken`)
            }
        })
    },

    checkEmail: (req,res) => {
        User.findOne({email: req.body.email}).then(result => {
            if(result == null){
                res.send(`Email does not exist`)
            }
            else{
                res.send(result)
            }
        })
    },

    login: (req,res) => {
        User.findOne({email: req.body.email}).then(result => {
            if(result == null){
                res.send(`User does not exist`)
            }
            else{
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

                if(isPasswordCorrect == true){
                    res.send({access : auth.createAccessToken(result)})
                }
                else{
                    res.send(`Incorrect Password`)
                }
            }
        })
    },

    detail: (req,res) => {
        const userData = auth.decode(req.headers.authorization)
        User.findById(userData.id).then(result => {
                res.send(result)
            
            
        })
    },

    update: (req,res) => {
        User.findByIdAndUpdate({_id: req.params.id}, {$set: {
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    password: bcrypt.hashSync(req.body.password, 10),
                    mobileNo: req.body.mobileNo,
                    isAdmin: req.body.isAdmin}}).then(result => {
                    res.send(`User Updated`)
                })  
    },

    delete: (req,res) => {
        User.findByIdAndDelete(req.body.id).then(result => {
            if(result == null){
                res.send(`No User Found`)
            }
            else{
                res.send(result)
            }
        })
    },

    enroll: async (req,res) => {
       
        let isUserUpdated = await User.findById(req.body.userId).then(result => {

            result.enrollments.push({courseId: req.body.courseId})

            return result.save().then((success, error) => {
                if(error){
                    return false;
                }
                else{
                    return true;
                }
            })
           
        })

        let isCourseUpdated = await Course.findById(req.body.courseId).then(result => {
            result.enrollees.push({userId: req.body.userId})

            return result.save().then((success, error) => {
                if(error){
                    return false;
                }
                else{
                    return true;
                }
            })
        })

        if(isUserUpdated && isCourseUpdated){
            res.send(`Enrollment Successful`)
        }
        else{
            res.send(`Enrollment Failed`)
        }

       
    }






    // enroll: async (data) => {
       
    // if(data.isAdmin){
       
    //     let isUserUpdated = await User.findById(data.userId).then(user => {
    //         // add the courseId in the user's enrollment array
    //         user.enrollments.push({courseId: data.courseId});

    //         // save the updated user informatio on the database
    //         return user.save().then((success, error) => {
    //             if(error){
    //                 return false;
    //             }
    //             else{
    //                 return true;
    //             }
    //         })
    //     })

    //     // Add the user Id in the enrollees array of the course
    //     let isCourseUpdated = await Course.findById(data.courseId).then(course => {
    //         course.enrollees.push({userId: data.userId});
    //         return course.save().then((success, error) => {
    //             if(error){
    //                 return false;
    //             }
    //             else{
    //                 return true;
    //             }
    //         })
    //     })
    //     // condition if user and course have been updated
    //     if(isUserUpdated && isCourseUpdated){
    //     return true;
    //     }
    //     else{
    //         return false;
    //     }
    // }
    // else{
    //     return `Admin required`;
    // }
    // }
    

}