const express = require(`express`)
const app = express()
const mongoose = require(`mongoose`)
const port = 4000

app.use(express.json())
app.use(express.urlencoded({extended:true}))

const userRoute = require(`./router/userRoute`)
app.use(`/user`, userRoute)
const courseRoute = require(`./router/courseRoute`)
app.use(`/course`, courseRoute)


mongoose.connect(`mongodb+srv://admin:admin@wdc028-course-booking.mgfy3.mongodb.net/practice6?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, () => console.log(`Connected to MongoDB`))

app.listen(port, () => console.log(`Now running under localhost: ${port}`))